import os
import re

rulesFile = "rules.txt"
moduleFile = "hangmanRules.py"

def parseRuleFileIntoModule(rulesFile, moduleFile):
    ruleModule = "from hangmansolverHelpers import guess,mostCommonLetter,mostCommonConsonant,mostCommonVowel,regex\nimport re\n"
    ruleModule = ruleModule + "import hangmansolverHelpers\n"
    ruleModule = ruleModule + "def ruleFunction(lettersGuessed, word):\n"
    ruleModule = ruleModule + "    guessMade = 0\n"
    ruleLineList = open(rulesFile, 'r').read().splitlines()
    for rule in ruleLineList:
        columnList = rule.split(' ')
        argumentList = "guessMade,lettersGuessed,word"
        if len(columnList) == 4:
            if (columnList[0] == 'if'):
                function = columnList[3]
                antecedent = columnList[1].replace("_",".") 
                antecedent = antecedent.replace("r'","")
                antecedent = antecedent.replace("'","")
                if function[0:5] == "guess":
                    argumentList = "guessMade, '" + str(function[6]) + "', lettersGuessed"
                    function = "guess"
                ruleLine = "    if (guessMade != 1) and (regex(\"" + str(antecedent) + "\", word) == 1):\n"
                ruleLine = ruleLine + "        guessMade = " + str(function) + "(" + str(argumentList) + ")\n"
        if len(columnList) == 1:
           ruleLine = "    guessMade = " + str(columnList[0]) + "(" + str(argumentList) + str(")\n")
        ruleModule = ruleModule + ruleLine + "\n"
    moduleFileHandle = open(moduleFile,'w')
    moduleFileHandle.write(ruleModule)
    moduleFileHandle.close()


parseRuleFileIntoModule(rulesFile, moduleFile)

from hangmanRules import ruleFunction   # ruleFunction is result of parsing rules.txt  
                                        #   Note there is no error or syntax checking.

lettersGuessed = []
print "Enter the number of letters"
numLetters = raw_input('')
# Initialize guessedWord
#   guessedWord = "ch__ge" for change, etc
guessedWord = ""
for i in range(0,int(numLetters)):
    guessedWord = guessedWord + "_"
    
for letter in guessedWord:
    if letter != "_":
        lettersGuessed.append(letter)
        
letterToGuess = ""
while "_" in guessedWord:
    letters="You have guessed: "
    for letter in lettersGuessed: 
        letters += str(letter) + ","
    print str(letters)
    ruleFunction(lettersGuessed,guessedWord)
    print "# of guesses: " + str(len(lettersGuessed))
    if len(lettersGuessed) >= 1:
        letterToGuess = lettersGuessed[len(lettersGuessed)-1]
    print "Guess the letter " + str(letterToGuess)
    print "Enter 'guessedWord'"
    newGuessedWord = raw_input('')
    if newGuessedWord != "miss":
        guessedWord = newGuessedWord
    
