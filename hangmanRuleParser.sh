#!/bin/bash
RULEFILE=$1
PARSEDFILE=$2
echo "from hangmansolverHelpers import regex, guess, mostCommonLetter, mostCommonConsonant, mostCommonVowel" > $PARSEDFILE
echo "def ruleFunction(lettersGuessed, word):" >> $PARSEDFILE
echo "    guessMade=0" >> $PARSEDFILE
cat $RULEFILE | grep "if.*mostCommon" | sed 's/if \(r.*\) then \(mostCommon.*\)/    if (guessMade != 1) and regex(\1, word):\n        guessMade=\2(guessMade, lettersGuessed, word)/' >> $PARSEDFILE
cat $RULEFILE | grep "if.*guess" | sed 's/if \(r.*\) then guess(\(.\))/    if (guessMade != 1) and regex(\1, word):\n        guessMade=guess("\2", guessMade, lettersGuessed)/' >> $PARSEDFILE
cat $RULEFILE | grep -v "if" | sed 's/\(.*\)/    guessMade=\1(guessMade, lettersGuessed, word)/' >> $PARSEDFILE
