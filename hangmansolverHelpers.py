import re

def guess(guessMade, probableLetter,lettersGuessed):
    print "guess(" + str(guessMade) + "," + str(probableLetter) 
    if (guessMade == 0):
        if probableLetter not in lettersGuessed:
            print "Guess the letter: " + str(probableLetter)
            lettersGuessed.append(probableLetter)
            return 1
    return 0

def mostCommonLetter(guessMade, lettersGuessed, guessedWord):
    print "mostCommonLetteR"
    if (guessMade == 0):
        regexGuessedWord = str("^") + guessedWord.replace("_",".") + "$"
        #print "regexGuessedWord = " + str(regexGuessedWord) 
        matchingWords = []
        wordsFileObject = open("words")
        for line in wordsFileObject:
            #print "regexGuessedWord = " + str(regexGuessedWord) + " line = " + str(line)
            matchObject = re.match( regexGuessedWord, line)
            if (matchObject):
                #print "matchingWords.group() = " + str(matchObject.group())
                matchingWords.append(str(matchObject.group()))
        if len(matchingWords) <= 10:
            for matchedWord in matchingWords:
                for letter in matchedWord:
                    if letter not in lettersGuessed:
                        return guess(guessMade,letter, lettersGuessed)
            
        
        print "len(matchingWords) = " + str(len(matchingWords))
        mostProbableLetters = ["e", "t", "a", "o", "i", "n", "s", "h", "r", "d", "l", "c", "u", "m", "w", "f", "g", "y", "p", "b", "v", "k", "j", "x", "q", "z"]
        for probableLetter in mostProbableLetters:
            if probableLetter not in lettersGuessed:
                return guess(guessMade,probableLetter, lettersGuessed)
    return 0

def mostCommonConsonant(guessMade, lettersGuessed, guessedWord):
    print "mostCommonConsonant"
    if (guessMade == 0):
        mostProbableLetters = ["t", "n", "s", "h", "r", "d", "l", "c", "m", "w", "f", "g", "y", "p", "b", "v", "k", "j", "x", "q", "z"]
        for probableLetter in mostProbableLetters:
            if probableLetter not in lettersGuessed:
                return guess(guessMade,probableLetter, lettersGuessed)
    return 0
    
def mostCommonVowel(guessMade, lettersGuessed, guessedWord):
    if (guessMade == 0):
        mostProbableLetters = ["e", "a", "o", "i", "u",]
        for probableLetter in mostProbableLetters:
            if probableLetter not in lettersGuessed:
                return guess(guessMade,probableLetter, lettersGuessed)
    return 0


def regex(expression, word):
    if (re.match( expression, word, re.M|re.I)):
        return 1
