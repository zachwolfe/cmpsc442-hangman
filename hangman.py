import sys
import random

NUMBER_OF_MISSES_ALLOWED = 9
NUMBER_OF_LETTERS = 26

class Hangman:
  def goodToGo(self, word):
    found = ' ' in word or '-' in word or "'" in word or ',' in word or '.' in word
    return not found and len(word) > 4 and len(word) < 9
  
  def __init__(self):
    self.guessedLetters = []
    self.numberOfMissesLeft = NUMBER_OF_MISSES_ALLOWED
    try:
      wordsFile = open('words.txt', 'r')
      self.words = wordsFile.read().splitlines()
      wordsFile.close()
    except:
      print "Could not open word file"
      sys.exit()
    
    #The original C++ code picks a random pivot, iterates to the end of the file and loops back from beginning in search of a suitable word
    #For simplicity here, I just try picking a random word every attempt. With 500 attempts available, there shouldn't be a big difference.
    random.seed()
    numberOfAttempts = 0
    while True:
      self.word = random.choice(self.words)
      if self.goodToGo(self.word):
        break
      
      if numberOfAttempts > 500:
        print "Could not find a word."
        sys.exit()
        
      numberOfAttempts += 1
    self.word = self.word.lower()
    #Strings are immutable, so store the guessed word as an array internally
    self.guessedWordArray = list("-" * len(self.word))
  
  def wordLength(self):
    return len(self.word)
  
  def checkAnswer(self, letter):
    hit = False
    answer = ''
    if letter in self.guessedLetters:
      answer = 'Already guessed'
    else:
      self.guessedLetters.append(letter)
      
      for letterIndex in range(len(self.word)):
        if self.word[letterIndex] == letter:
          self.guessedWordArray[letterIndex] = letter
          answer = 'Hit!'
          hit = True
    
    if not hit:
      if answer == "":
        answer = 'Miss'
      self.numberOfMissesLeft -= 1
    
    guessedWord = "".join(self.guessedWordArray)
    return answer, guessedWord, self.numberOfMissesLeft
  
  def keepGoing(self):
    guessedWord = "".join(self.guessedWordArray)
    if self.word == guessedWord:
      print "You win!"
      return False
    if self.numberOfMissesLeft <= 0:
      print "You lose!"
      print "The word was " + self.word
      return False
    return True

class Group2:
  def getLetter(self):
    line = raw_input('')
    if len(line) == 1:
        return line[0].lower()
    else:
        print "Invalid input"
        return ""
  def wordLength(self, wordLength):
    print "Processing: Word length is " + str(wordLength)
  
  def processResult(self, result, status, missesLeft):
    print "Processing"
    print "\tresult     [" + str(result) + "]"
    print "\tstatus     [" + status + "]"
    print "\tmissesLeft [" + str(missesLeft) + "]"
    
if __name__ == '__main__':
  hangman = Hangman()
  player = Group2()
  
  player.wordLength(hangman.wordLength())
  while hangman.keepGoing():
    print "Enter a letter:"
    letter = player.getLetter()
    if (letter != ""):
        result, status, missesLeft = hangman.checkAnswer(letter)
        player.processResult(result, status, missesLeft)
    print "--------------------------------"
